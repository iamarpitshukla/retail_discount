package com.assignment.discount.exceptions;

public class RetailDiscountException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1954707331398105327L;
	
	private String message;

	public RetailDiscountException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}


}
