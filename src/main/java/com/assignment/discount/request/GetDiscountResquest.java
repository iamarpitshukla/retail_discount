package com.assignment.discount.request;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class GetDiscountResquest {
	
	public GetDiscountResquest(@NotNull Long userId, String itemType, @NotNull double price) {
		super();
		this.userId = userId;
		this.itemType = itemType;
		this.price = price;
	}
	@NotNull
	private Long userId;

	private String itemType;
	@NotNull
	private double price;

}
