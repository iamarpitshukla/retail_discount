package com.assignment.discount.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.assignment.discount.enums.UserType;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity(name = "user")
public class User {

	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "type")
	private String type = UserType.NOR.name();

	@Column(name = "created")
	private Date created = new Date();

	@Column(name = "updated")
	private Date updated;

	@Column(name = "enabled")
	private boolean enabled;
	
	public User() {
		super();
	}

	public User(Long id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	public User(Long id, String type, Date created) {
		super();
		this.id = id;
		this.type = type;
		this.created = created;
	}
	
}
