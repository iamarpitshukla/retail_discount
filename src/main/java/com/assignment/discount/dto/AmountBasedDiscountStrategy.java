package com.assignment.discount.dto;

public class AmountBasedDiscountStrategy implements DiscountStrategy{


	@Override
	public double getDiscount(double price) {
		double discount = 0;
		while(price >= 100) {
			discount+=5;
			price-=100;
		}
		return discount;
	}
	
}
