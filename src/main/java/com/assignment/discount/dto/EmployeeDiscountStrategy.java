package com.assignment.discount.dto;

public class EmployeeDiscountStrategy implements DiscountStrategy{
	
	private int percentage = 30;
	
	public EmployeeDiscountStrategy() {
		super();
	}
	
	public EmployeeDiscountStrategy(int percentage) {
		super();
		this.percentage = percentage;
	}

	@Override
	public double getDiscount(double price) {
		return price*percentage/100;
	}
	
}