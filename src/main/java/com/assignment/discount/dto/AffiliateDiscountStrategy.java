package com.assignment.discount.dto;

public class AffiliateDiscountStrategy implements DiscountStrategy{

	private int percentage = 20;
	
	public AffiliateDiscountStrategy(int percentage) {
		super();
		this.percentage = percentage;
	}

	@Override
	public double getDiscount(double price) {
		return price*percentage/100;
	}
	
}
