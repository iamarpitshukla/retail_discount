package com.assignment.discount.dto;

public interface DiscountStrategy {
	
	double getDiscount(double price);

}
