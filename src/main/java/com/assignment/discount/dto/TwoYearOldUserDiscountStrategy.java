package com.assignment.discount.dto;

public class TwoYearOldUserDiscountStrategy implements DiscountStrategy{

	private int percentage = 5;
	
	public TwoYearOldUserDiscountStrategy(int percentage) {
		super();
		this.percentage = percentage;
	}

	@Override
	public double getDiscount(double price) {
		return price*percentage/100;
	}
	
}
