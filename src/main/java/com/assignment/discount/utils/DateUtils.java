package com.assignment.discount.utils;

import java.util.Date;

public class DateUtils {
	
	public static long getDateDiffInDays(Date current, Date dealtime) {
		long diff;
		diff = current.getTime() - dealtime.getTime();
		diff = diff / (24 * 60 * 60 * 1000);
		return diff;
	}
}
