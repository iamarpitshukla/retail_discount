package com.assignment.discount.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.discount.exceptions.RetailDiscountException;
import com.assignment.discount.request.GetDiscountResquest;
import com.assignment.discount.response.GetDiscountResponse;
import com.assignment.discount.services.DiscountService;


@RestController
@RequestMapping(value = "discount")
public class DiscountController {

	private static final Logger LOG = LoggerFactory.getLogger(DiscountController.class);
	
	@Autowired
	DiscountService disocuntService;

	@PostMapping(value = "/get/payable/amount")
	@ResponseStatus(HttpStatus.OK)
	public GetDiscountResponse getTransactions(@Validated @RequestBody GetDiscountResquest request) {
		GetDiscountResponse response = new GetDiscountResponse();
		LOG.info("Request to get payable amount :{}", request);
		try {
			response = disocuntService.getNetPayableAmount(request);
			response.setStatusCode(200);
			response.setMessage("success");
		}catch(RetailDiscountException re) {
			LOG.error("RetailDiscountException while fetching payable amount:{}", re);
			response.setMessage(re.getMessage());
			response.setStatusCode(201);
		}catch (Exception e) {
			LOG.error("Error while fetching payable amount:{}", e);
			response.setMessage("Something went wrong");
			response.setStatusCode(500);
		}
		return response;
	}
}
