package com.assignment.discount.services;

import com.assignment.discount.entity.User;

public interface UserService {

	User getUserById(Long id);

}
