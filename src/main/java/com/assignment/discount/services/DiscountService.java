package com.assignment.discount.services;

import com.assignment.discount.request.GetDiscountResquest;
import com.assignment.discount.response.GetDiscountResponse;

public interface DiscountService {

	GetDiscountResponse getNetPayableAmount(GetDiscountResquest request) throws Exception;

}
