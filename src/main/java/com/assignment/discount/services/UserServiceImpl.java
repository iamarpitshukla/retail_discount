package com.assignment.discount.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.discount.entity.User;
import com.assignment.discount.repo.UserRepo;

@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepo userRepo;
	
	@Override
	public User getUserById(Long id) {
		return userRepo.findUserById(id);
	}
}
