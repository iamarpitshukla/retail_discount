package com.assignment.discount.services;

import java.util.Date;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.discount.dto.AffiliateDiscountStrategy;
import com.assignment.discount.dto.AmountBasedDiscountStrategy;
import com.assignment.discount.dto.DiscountStrategy;
import com.assignment.discount.dto.EmployeeDiscountStrategy;
import com.assignment.discount.dto.GroceriesDiscountStrategy;
import com.assignment.discount.dto.TwoYearOldUserDiscountStrategy;
import com.assignment.discount.entity.User;
import com.assignment.discount.enums.ItemType;
import com.assignment.discount.enums.UserType;
import com.assignment.discount.exceptions.RetailDiscountException;
import com.assignment.discount.request.GetDiscountResquest;
import com.assignment.discount.response.GetDiscountResponse;
import com.assignment.discount.utils.DateUtils;

@Service("discountService")
public class DiscountServiceImpl implements DiscountService{
	
	@Autowired
	UserService userService;
	
	private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(DiscountServiceImpl.class);

	
	public DiscountServiceImpl(UserService userService2) {
		this.userService = userService2;
	}

	@Override
	public GetDiscountResponse getNetPayableAmount(GetDiscountResquest request) throws Exception {
		GetDiscountResponse response = new GetDiscountResponse();
		User user = userService.getUserById(request.getUserId());
		if (user == null) {
			throw new RetailDiscountException("User not found");
		}
		DiscountStrategy discountStrategy = getDiscountStrategy(user, request.getItemType());
		double discount = discountStrategy.getDiscount(request.getPrice());
		response.setNetPayableAmount(request.getPrice() - discount);
		return response;
	}

	private DiscountStrategy getDiscountStrategy(User user, String itemType) {
		DiscountStrategy discountStrategy;
		if (ItemType.GROCERIES.name().equalsIgnoreCase(itemType)) {
			discountStrategy = new GroceriesDiscountStrategy();
		}else if (user.getType().equalsIgnoreCase(UserType.EMP.name())) {
			discountStrategy = new EmployeeDiscountStrategy(30);
		} else if (user.getType().equalsIgnoreCase(UserType.AFF.name())) {
			discountStrategy = new AffiliateDiscountStrategy(20);
		} else if (DateUtils.getDateDiffInDays(new Date(), user.getCreated()) > 730) {
			discountStrategy = new TwoYearOldUserDiscountStrategy(5);
		} else {
			discountStrategy = new AmountBasedDiscountStrategy();
		}
		return discountStrategy;
	}

}
