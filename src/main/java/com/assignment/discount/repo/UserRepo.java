package com.assignment.discount.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.assignment.discount.entity.User;

@Repository
public interface UserRepo extends CrudRepository<User, Long>{
	
	User findUserById(@Param("id") Long id);

}
