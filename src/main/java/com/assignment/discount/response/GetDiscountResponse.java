package com.assignment.discount.response;

import lombok.Data;

@Data
public class GetDiscountResponse extends ApiResponse{

	private double netPayableAmount;
	
}
