package com.assignment.discount.response;

import lombok.Data;

@Data
public class ApiResponse {
	private int statusCode;
	private String message;
}
