package com.assignment.discount;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.assignment.discount.entity.User;
import com.assignment.discount.enums.ItemType;
import com.assignment.discount.enums.UserType;
import com.assignment.discount.request.GetDiscountResquest;
import com.assignment.discount.services.DiscountServiceImpl;
import com.assignment.discount.services.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MockitoControllerTest {
	
    @Test
	public void testCase1WithMock() throws Exception {
		UserService userService = mock(UserService.class);
		GetDiscountResquest req = new GetDiscountResquest(1l, "", 1000);
		when(userService.getUserById(req.getUserId())).thenReturn(new User(1l, UserType.EMP.name()));
		DiscountServiceImpl discountService = new DiscountServiceImpl(userService);
		double result = discountService.getNetPayableAmount(req).getNetPayableAmount();
		assertEquals(700, result);

	}

	@Test
	public void testCase2WithMock() throws Exception {
		UserService userService = mock(UserService.class);
		GetDiscountResquest req = new GetDiscountResquest(1l,"",1000);
		when(userService.getUserById(req.getUserId())).thenReturn(new User(1l, UserType.AFF.name()));
		DiscountServiceImpl discountService = new DiscountServiceImpl(userService);
		double result = discountService.getNetPayableAmount(req).getNetPayableAmount();
		assertEquals(800, result);
	}

	@Test
	public void testCase4WithMock() throws Exception {
		UserService userService = mock(UserService.class);
		GetDiscountResquest req = new GetDiscountResquest(1l,"",990);
		when(userService.getUserById(req.getUserId())).thenReturn(new User(1l, UserType.NOR.name(), new Date(1480101343)));
		DiscountServiceImpl discountService = new DiscountServiceImpl(userService);
		double result = discountService.getNetPayableAmount(req).getNetPayableAmount();
		assertEquals(940.5, result);
	}

	@Test
	public void testCase5WithMock() throws Exception {
		UserService userService = mock(UserService.class);
		GetDiscountResquest req = new GetDiscountResquest(1l,"",1000);
		req.setItemType(ItemType.GROCERIES.name());
		when(userService.getUserById(req.getUserId())).thenReturn(new User(1l, UserType.EMP.name()));
		DiscountServiceImpl discountService = new DiscountServiceImpl(userService);
		double result = discountService.getNetPayableAmount(req).getNetPayableAmount();
		assertEquals(1000, result);
	}
	
	@Test
	public void testCase6WithMock() throws Exception {
		UserService userService = mock(UserService.class);
		GetDiscountResquest req = new GetDiscountResquest(1l,"",990);
		when(userService.getUserById(req.getUserId())).thenReturn(new User(1l, UserType.NOR.name()));
		DiscountServiceImpl discountService = new DiscountServiceImpl(userService);
		double result = discountService.getNetPayableAmount(req).getNetPayableAmount();
		assertEquals(945, result);
	}
}
