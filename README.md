# retail_discount

Key Classes and responsibilities

User.java (entity class): used to store user information in database.

DiscountController: Contains api end point 

DiscountServiceImpl: service which gives net payable ammount based on user's details

UserServiceImpl: Deals with user related information

UserRepo: Repository to fetch user information from database


application.properties:

This file holds the information related to configurations. Primarily this has infomation about db connection and logs file.


MockitoControllerTest: This class holds the test cases for the api development


How to run on your local:


step 1. create mysql db discount

step 2. create table user in db discount

step 3. clone repo

step 4. Run junit test caes using command mvn test

stpe 5. command to execute spring boot code:

		 mvn spring-boot:run

		 Now server is running on port 1001 (defined in application.properties)


To generate Sonar html report hit command (my sonar host url is defined in pom )

mvn sonar:sonar -Dsonar.analysis.mode=preview -Dsonar.issuesReport.html.enable=true





